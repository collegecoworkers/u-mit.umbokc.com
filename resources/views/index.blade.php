@extends('layout.app')
@section('content')

<div class="main_content">
	<div class="blog_section">
		<div class="section_panel d-flex flex-row align-items-center justify-content-start">
			<div class="section_title">
				Последние новости
				@isset ($curr_item)
				    : {{$curr_item->title}}
				@endisset
			</div>
		</div>
		<div class="section_content">
			<div class="" ea-s:chs='d:ib'>
				@php
					$i = 0;
				@endphp
				@foreach ($data as $item)
					<div class="card asdasdasdsad">
						@if (isset($item->image) and trim($item->image) != '' )
							<img class="card-img-top" src="{{$item->image}}" alt="{{$item->image}}">
						@endisset
						<div class="card-body">
							<div class="card-title"><a target="_blank" href="{{ $item->link }}">{{ $item->title }}</a></div>
							<small class="post_meta">{{ date('F j, Y \a\t h:i A', strtotime($item->pubDate)) }}</small>
						</div>
					</div>

					@php
						$i++;
					@endphp
					@if ($i % 3 == 0)
						<div class="clearfix"></div>
					@endif
				@endforeach
			</div>
		</div>
	</div>
</div>
<style ea-s:chs='d:n'> .asdasdasdsad{ width: 29%; margin: 20px 10px; vertical-align: middle; } </style>

@endsection
