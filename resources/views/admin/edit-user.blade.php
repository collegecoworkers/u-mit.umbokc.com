@extends('layout.app')
@section('content')

<div class="col-lg-9">
	<div class="main_content">
		<div class="" ea-s='m:t:big'>
			<h3>Изменить пользователя</h3>
			<br>
			{!! Form::open(array('url' => '/update-user/' .$model->id )) !!}
			<div class="form-group">
				{!! Form::text('full_name', $model->full_name, ['placeholder' => 'Имя', 'class' => 'form-control']) !!}
				@if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif
			</div>
			<div class="form-group">
				{!! Form::text('name', $model->name, ['placeholder' => 'Логин', 'class' => 'form-control']) !!}
				@if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif
			</div>
			<div class="form-group">
				{!! Form::text('email', $model->email, ['placeholder' => 'Почта', 'class' => 'form-control']) !!}
				@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
			</div>
			<div class="form-group">
				<label>Тип</label>
				{!! Form::select('is_admin', ['0' => 'Пользователь', '1' => 'Админ'], $model->is_admin, ['class' => 'form-control']) !!}
				@if ($errors->has('is_admin')) <span class="help-block"><strong>{{ $errors->first('is_admin') }}</strong></span> @endif
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">Сохранить</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


@endsection
