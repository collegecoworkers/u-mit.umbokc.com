@extends('layout.app')
@section('content')

<div class="main_content">
  <div class="" ea-s='m:t:big'>
    <h3>Список пользователей</h3>
    <br>
    <br>
    <table class="table">
      <thead>
        <tr>
          <td>#</td>
          <td>Имя</td>
          <td>Логин</td>
          <td>Почта</td>
          <td>Тип</td>
          <td>Действия</td>
        </tr>
      </thead>
      <tbody>
        @foreach ($users as $item)
        <tr>
          <td>{{$item->id}}</td>
          <td>{{$item->full_name}}</td>
          <td>{{$item->name}}</td>
          <td>{{$item->email}}</td>
          <td>{{$item->getRole()}}</td>
          <td>
            <a c#7 td:n td:u@hov href="/edit-user/{{$item->id}}"><i class="fa fa-edit"></i></a>
            <a c#7 td:n td:u@hov href="/delete-user/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>


@endsection
