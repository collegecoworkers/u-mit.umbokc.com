@extends('layout.auth')
@section('content')

<div class="home">
	<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/regular.jpg') }}" data-speed="0.8"></div>
	<div class="home_content" style="top: 6%">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="post_comment">
						<div class="contact_form_container">
							<h3>Регистрация</h3>
							<form method="post" action="{{ route('register') }}">
								{{ csrf_field() }}

								<input type="text" name="full_name" class="contact_text" placeholder="Ваше полное имя" required="required">
								@if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif

								<input type="text" name="name" class="contact_text" placeholder="Придумайте логин" required="required">
								@if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif

								<input type="email" name="email" class="contact_text" placeholder="Email" required="required">
								@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
								
								<input type="password" name="password" class="contact_text" placeholder="Пароль" required="required">
								@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif

								<input type="password" name="password_confirmation" class="contact_text" placeholder="Повторите пароль" required="required">
								@if ($errors->has('password_confirmation')) <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span> @endif

								<button type="submit" class="contact_button">Войти</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>    
	</div>
</div>

@endsection
