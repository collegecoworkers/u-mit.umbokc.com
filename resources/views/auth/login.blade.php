@extends('layout.auth')
@section('content')

<div class="home">
	<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{ asset('assets/images/regular.jpg') }}" data-speed="0.8"></div>
	<div class="home_content" style="top: 25%">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 offset-lg-3">
					<div class="post_comment">
						<div class="contact_form_container">
							<h3>Вход</h3>
							<form method="post" action="{{ route('login') }}">
								{{ csrf_field() }}
								<input type="email" name="email" class="contact_text" placeholder="Email" required="required">
								@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
								<input type="password" name="password" class="contact_text" placeholder="Пароль" required="required">
								@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
								<button type="submit" class="contact_button">Войти</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>    
	</div>
</div>

@endsection
