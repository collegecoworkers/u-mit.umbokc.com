@php
use App\{
	Item,
	User
};

$items = Item::getsBy('user_id', User::curr()->id);
$user = User::curr();
@endphp
<!DOCTYPE html>
<html lang="en" ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=2">
	<link rel="stylesheet" href="{{ asset('assets/styles/bootstrap4/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/styles/main_styles.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/styles/responsive.css') }}">
	<style>
		.s-item{
			padding: 3px 0;
		}
	</style>
</head>
<body>
	<div class="super_container">
		<header class="header">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="header_content d-flex flex-row align-items-center justify-content-start">
							<div class="logo"><a href="#">avision</a></div>
							<nav class="main_nav">
								<ul>
									<li><a href="/">Главная</a></li>
									@if (User::isAdmin())
										<li><a href="/users">Пользователи</a></li>
									@endif
									<li><a href="/all">Подписки</a></li>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
									<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выход</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="home" ea-s=''>
			<div ea-s='h:100p' class="home_background parallax-window" style="background-image: url(/assets/images/regular.jpg);">
				<div class="home_content" style="top: 40%;">
					<div class="container">
						<div class="row">
							<div class="col-lg-12" ea-s='ta:c'>
								<h1>Управление новостными подписками</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="page_content">
			<div class="container">
				<div class="row row-lg-eq-height">

					<div class="col-lg-9">
						@yield('content')
					</div>

					<div class="col-lg-3" >
						<div class="sidebar" ea-j='mn-h=500px'>
							<div class="sidebar_background"></div>
							<div class="sidebar_section">
								<div><h5>Вы авторизированны как: <br> {{ $user->full_name }}</h5></div>
								<div class="sidebar_title_container">
									<div class="sidebar_title">Ваши подписки</div>
								</div>
								<div class="" ea-s='p:t'>
									<div class="">
										<ul>
											@foreach ($items as $item)
											<li class="s-item">
												<a href="/item/{{ $item->id }}">{{ $item->title }}</a>
											</li>
											@endforeach
											<li ea-s='p:t'><a href="/add">+ Добавить</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<footer class="footer">
			<div class="container">
				<div class="row row-lg-eq-height">
					<div class="col-lg-12">
						<div class="footer_content">
							<div class="copyright">
								Copyright &copy;
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>

	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>
	<script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('assets/styles/bootstrap4/popper.js') }}"></script>
	<script src="{{ asset('assets/styles/bootstrap4/bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/easing/easing.js') }}"></script>
	<script src="{{ asset('assets/js/custom.js') }}"></script>
</body>
</html>
