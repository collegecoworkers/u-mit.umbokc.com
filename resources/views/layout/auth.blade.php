<!DOCTYPE html>
<html ea>
<head>
  <title>{{ config('app.name', 'Laravel') }}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=2">
  <link rel="stylesheet" href="{{ asset('assets/styles/bootstrap4/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/styles/contact.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/styles/contact_responsive.css') }}">
</head>
<body>

  <div class="super_container">

  	<header class="header">
  		<div class="container">
  			<div class="row">
  				<div class="col">
  					<div class="header_content d-flex flex-row align-items-center justify-content-start">
  						<div class="logo"><a href="/">{{ config('app.name', 'Laravel') }}</a></div>
  						<nav class="main_nav">
  							<ul>
  								<li><a href="{{ route('login') }}">Войти</a></li>
  								<li><a href="{{ route('register') }}">Зарегистрироваться</a></li>
  							</ul>
  						</nav>
  					</div>
  				</div>
  			</div>
  		</div>
  	</header>


    @yield('content')

    <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="footer_content">
              <div class="copyright">
                &copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </div>

  <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
  <script src="{{ asset('assets/styles/bootstrap4/popper.js') }}"></script>
  <script src="{{ asset('assets/styles/bootstrap4/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/plugins/parallax-js-master/parallax.min.js') }}"></script>
  <script src="{{ asset('assets/js/contact.js') }}"></script>
</body>
</html>
