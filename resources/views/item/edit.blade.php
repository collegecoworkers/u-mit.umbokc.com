@extends('layout.app')
@section('content')

<div class="col-lg-9">
	<div class="main_content">
		<div class="" ea-s='m:t:big'>
			<h3>Изменить подписку</h3>
			<br>
			<form method="post" action="/update/{{ $model->id }}" >
				{{ csrf_field() }}
				<div class="form-group">
					<label for="id__title">Название</label>
					<input type="text" class="form-control" id="id__title" name="title" placeholder="Название" required="" value="{{ $model->title }}">
				</div>
				<div class="form-group">
					<label for="id__rss">RSS</label>
					<input type="text" class="form-control" id="id__rss" name="rss" placeholder="RSS" required="" value="{{ $model->rss }}">
				</div>
				<button type="submit" class="btn btn-success">Сохранить</button>
			</form>
		</div>
	</div>
</div>


@endsection
