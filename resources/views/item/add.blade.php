@extends('layout.app')
@section('content')


<div class="main_content">
	<div class="" ea-s='m:t:big'>
		<h3>Добавить подписку</h3>
		<br>
		<form method="post" action="/create">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="id__title">Название</label>
				<input type="text" class="form-control" id="id__title" name="title" placeholder="Название" required="">
			</div>
			<div class="form-group">
				<label for="id__rss">RSS</label>
				<input type="text" class="form-control" id="id__rss" name="rss" placeholder="RSS" required="">
			</div>
			<button type="submit" class="btn btn-success">Отправить</button>
		</form>
	</div>
</div>



@endsection
