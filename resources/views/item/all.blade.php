@extends('layout.app')
@section('content')


<div class="main_content">
	<div class="" ea-s='m:t:big'>
		<h3>Список ваших подписок</h3>
		<br>
		<div ea-s='p:t'><a href="/add">+ Добавить</a></div>
		<br>
		<table class="table">
			<thead>
				<tr>
					<td>#</td>
					<td>Название</td>
					<td>Rss</td>
					<td>Действия</td>
				</tr>
			</thead>
			<tbody>
				@foreach ($items as $item)
				<tr>
					<td>{{$item->id}}</td>
					<td>{{$item->title}}</td>
					<td>{{$item->rss}}</td>
					<td>
						<a c#7 td:n td:u@hov href="/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>
						<a c#7 td:n td:u@hov href="/delete/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>



@endsection
