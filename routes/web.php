<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/item/{id}', 'SiteController@Item');

Route::get('/all', 'ItemController@All');
Route::get('/add', 'ItemController@Add');
Route::get('/edit/{id}', 'ItemController@Edit');
Route::get('/delete/{id}', 'ItemController@Delete');
Route::post('/create', 'ItemController@Create');
Route::post('/update/{id}', 'ItemController@Update');

Route::get('/users', 'AdminController@Users');
Route::get('/edit-user/{id}', 'AdminController@EditUser');
Route::get('/delete-user/{id}', 'AdminController@DeleteUser');
Route::post('/update-user/{id}', 'AdminController@UpdateUser');


