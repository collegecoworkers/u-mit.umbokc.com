<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [ 'full_name', 'name', 'email', 'password' ];

	protected $hidden = [ 'password', 'remember_token' ];


	public function getRole() {
		return self::getRoleOf($this->is_admin);
	}

	public static function getRoles(){
		return [
			'0' => 'Пользователь',
			'1' => 'Админ',
		];
	}

	public static function getRoleOf($r){
		$roles = self::getRoles();
		if(array_key_exists($r, $roles)) 
			return $roles[$r];
		return $roles[0];
	}

	public static function isAdmin() { return self::curr()->is_admin == 1; }
	public static function isUser() { return !self::isAdmin(); }
	public static function curr() { return auth()->user(); }

	public static function allArr($field = 'name'){ return F::toArr(self::all(), $field, 'id'); }

	public static function getById($val){ return self::getBy('id', $val); }
	public static function getBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->first(); else return self::queryBy([$col => $val])->first(); }
	public static function getsBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->get(); else return self::queryBy([$col => $val])->get(); }
	public static function queryBy($arr){ return self::where($arr); }
}
