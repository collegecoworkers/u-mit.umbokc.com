<?php
namespace App;

class Item extends MyModel{
  public $timestamps = false;

  public function getData() {
    $xml = @simplexml_load_file($this->rss);

    if($xml===false) return [];

    return $xml->xpath('//item');
  }

  static function getAllData($curr = null){
    if ($curr == null) {
      $items = Item::getsBy('user_id', User::curr()->id);
    } else {
      $items = Item::getsBy('id', $curr);
    }

    $data = [];
    $tmp_data = [];

    foreach ($items as $item) {
      $tmp_data = array_merge($tmp_data, $item->getData());
    }

    array_map(function($item) use (&$data, &$i) {
      $obj = new \StdClass;
      $obj->title = (string) $item->title;
      $obj->link = (string) $item->link;
      $obj->description = substr($item->description,0,198) . '...';
      $obj->pubDate = (string) $item->pubDate;
      $obj->image = F::getImage($item);
      $data[] = $obj;
      unset($obj);
    }, $tmp_data);

    usort($data, function( $a, $b ) {
      return strtotime($a->pubDate) - strtotime($b->pubDate);
    });

    $data = array_slice($data, 0, 51);

    return $data;
  }

}
