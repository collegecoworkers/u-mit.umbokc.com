<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Item,
	User
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {

		return view('index')->with([
			'data' => Item::getAllData(),
		]);
	}
	public function Item($id) {
		$curr_item = Item::getBy('id', $id);
		return view('index')->with([
			'data' => Item::getAllData($curr_item->id),
			'curr_item' => $curr_item,
		]);
	}
}
