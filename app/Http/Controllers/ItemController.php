<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Item,
	User
};

class ItemController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function All() {
		$items = Item::getsBy('user_id', User::curr()->id);
		return view('item.all')->with([
			'items' => $items,
		]);
	}
	public function Add() {
		$item = new Item();
		return view('item.add')->with([
			'model' => $item,
		]);
	}
	public function Edit($id) {
		$item = Item::where('id', $id)->first();
		return view('item.edit')->with([
			'model' => $item,
		]);
	}
	public function Create(Request $request) {

		$model = new Item();

		$model->title = request()->title;
		$model->rss = request()->rss;
		$model->user_id = User::curr()->id;

		$model->save();

		return redirect('/');
	}
	public function Update($id, Request $request) {
		$model = Item::where('id', $id)->first();

		$model->title = request()->title;
		$model->rss = request()->rss;

		$model->save();
		return redirect()->to('/');
	}
	public function Delete($id) {
		Item::where('id', $id)->delete();
		return redirect()->to('/');
	}
}
